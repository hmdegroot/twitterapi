﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterApi.Controllers;
using TwitterApi.DataAccessor;
using TwitterApi.DataAccessor.TwitterClient;
using TwitterApi.Managers;

namespace TwitterApi.IntegrationTests
{
    [TestClass]
    public class SearchIntegrationTests
    {
        private ITwitterSearchService _twitterSearchService;
        private ITwitterSearchManager _twitterSearchManager;
        private ITwitterClient _twitterClient;

        [TestInitialize]
        public void Initialize()
        {
            _twitterClient = new TwitterClient();
            _twitterSearchService = new TwitterSearchService(_twitterClient);
            _twitterSearchManager = new TwitterSearchManager(_twitterSearchService);
        }

        [TestMethod]
        public async Task SearchIntegrationTestNumeroUno()
        {
            var controller = new TwitterSearchController(_twitterSearchManager);
            var result = await controller.Search("mittens");
            var mittens = result.Status.FirstOrDefault(x => x.Text.Contains("mittens"));

            Assert.IsTrue(mittens != null); 
            Assert.IsTrue(result.Status.Count > 10);
        }

        [TestMethod]
        public async Task TwitterSearchDataAccessor_SearchTweets_CanGetResultsWithSpecialCharacters()
        {
            var controller = new TwitterSearchController(_twitterSearchManager);
            var result = await controller.Search("#Nebraska");
            var nebraska = result.Status.FirstOrDefault(x => x.Text.Contains("#nebraska"));
            var firstResult = result.Status[1];

            Assert.IsTrue(nebraska != null);
            Assert.IsTrue(firstResult.Text.Length > 0);
            Assert.IsTrue(result.Status.Count > 10);
        }

        [TestMethod]
        public async Task TwitterSearchDataAccessor_SearchTweets_EmptySearchReturnsNull()
        {
            var controller = new TwitterSearchController(_twitterSearchManager);
            var result = await controller.Search("");
        
            Assert.IsNull(result);
        }
    }
}
