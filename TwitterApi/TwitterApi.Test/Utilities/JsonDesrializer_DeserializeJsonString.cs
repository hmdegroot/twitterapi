﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TwitterApi.Utilities;

namespace TwitterApi.Test
{
    [TestClass]
    public class JsonDesrializer_DeserializeJsonString
    {
        [TestMethod]
        public void JsonDesrializer_DeserializeJsonString_CanDeserializeCompleteString()
        {
            var jsonString = "{\"name\":\"Red\",\"number\":3,\"phoneNumber\":\"123-456-7891\",\"favoriteColor\":\"Blue\"}";
            var deserializedJson = JsonDeserializer.DeserializeJsonString<TestModel>(jsonString);

            Assert.AreEqual(deserializedJson.FavoriteColor, "Blue");
            Assert.AreEqual(deserializedJson.Number, 3);
        }

        [TestMethod]
        public void JsonDesrializer_DeserializeJsonString_WillWorkWithMissingFields()
        {
            var jsonString = "{\"name\":\"Red\",\"phoneNumber\":\"123-456-7891\",\"favoriteColor\":\"Blue\",\"bestFriend\":\"Rosco\"}";
            var deserializedJson = JsonDeserializer.DeserializeJsonString<TestModel>(jsonString);

            Assert.AreEqual(deserializedJson.FavoriteColor, "Blue");
            Assert.AreEqual(deserializedJson.Number, 0);
        }
    }

    public class TestModel
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public string PhoneNumber { get; set; }
        public string FavoriteColor { get; set; }
    }
}
