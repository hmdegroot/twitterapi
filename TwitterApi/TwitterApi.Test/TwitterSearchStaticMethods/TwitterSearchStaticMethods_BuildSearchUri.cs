using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using TwitterApi.DataAccessor;
using System.Linq;
using System.Net.Http;
using System;
using TwitterApi.DataAccessor.TwitterClient;
using TwitterApi.Managers;
using Moq;

namespace TwitterApi.Test
{
    [TestClass]
    public class TwitterSearchStaticMethods_BuildSearchUri
    {
        [TestMethod]
        public void TwitterSearchStaticManager_BuildSearchUri_CanBuildUri()
        {
            //arrange
            var query = "somethingToSearch";

            //act
            var result = TwitterSearchStaticMethods.BuildSearchUri(query);

            //assert
            Assert.AreEqual(result, "https://api.twitter.com/1.1/search/tweets.json?q=somethingToSearch");
        }

        [TestMethod]
        public void TwitterSearchStaticManager_BuildSearchUri_CanHandleHashtag()
        {
            //arrange
            var query = "#trendingnow";

            //act
            var result = TwitterSearchStaticMethods.BuildSearchUri(query);

            //assert
            Assert.AreEqual(result, "https://api.twitter.com/1.1/search/tweets.json?q=%23trendingnow");
        }

        [TestMethod]
        public void TwitterSearchStaticManager_BuildSearchUri_CanHandleAt()
        {
            //arrange
            var query = "@youknowwho";

            //act
            var result = TwitterSearchStaticMethods.BuildSearchUri(query);

            //assert
            Assert.AreEqual(result, "https://api.twitter.com/1.1/search/tweets.json?q=%40youknowwho");
        }
        
        [TestMethod]
        public void TwitterSearchStaticManager_BuildSearchUri_CanHandleEmptyRequest()
        {
            //arrange
            var query = "";

            //act
            var result = TwitterSearchStaticMethods.BuildSearchUri(query);

            //assert
            Assert.AreEqual(result, "https://api.twitter.com/1.1/search/tweets.json?q=");
        }

    }
}
