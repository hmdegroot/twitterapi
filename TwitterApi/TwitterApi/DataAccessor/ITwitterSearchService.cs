﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using TwitterApi.Models;

namespace TwitterApi.DataAccessor
{
    public interface ITwitterSearchService
    {
        public Task<HttpResponseMessage> SearchTweets(string query);
    }
}
