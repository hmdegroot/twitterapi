﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace TwitterApi.DataAccessor.TwitterClient
{
    public interface ITwitterClient
    {
        public HttpClient GetTwitterClient();
    }
}
