﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TwitterApi.DataAccessor.TwitterClient;

namespace TwitterApi.DataAccessor
{
    public class TwitterSearchService : ITwitterSearchService
    {
        private readonly ITwitterClient _twitterQueryMethods;

        public TwitterSearchService(ITwitterClient twitterQueryMethods)
        {
            _twitterQueryMethods = twitterQueryMethods;
        }

        //This is likely also reusible were I to add another endpoint. 
        public async Task<HttpResponseMessage> SearchTweets(string requestUri)
        {
            var client = _twitterQueryMethods.GetTwitterClient();

            var response = await client.GetAsync(requestUri);
            response.EnsureSuccessStatusCode();

            return response;
        }
    }
}
