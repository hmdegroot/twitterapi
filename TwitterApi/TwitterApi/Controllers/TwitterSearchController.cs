﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TwitterApi.Managers;
using TwitterApi.Models;

namespace TwitterApi.Controllers
{
    [ApiController]
    public class TwitterSearchController : Controller
    {
        private readonly ITwitterSearchManager _twitterSearchManager;

        public TwitterSearchController(ITwitterSearchManager twitterSearchManager)
        {
            _twitterSearchManager = twitterSearchManager;
        }

        [HttpGet("search/{query}")]
        public async Task<TwitterSearchResult> Search(string query)
        {
            try
            {
                var result = await _twitterSearchManager.GetTwitterSearchResults(query);
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
