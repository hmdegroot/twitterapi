﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TwitterApi.Models
{
    public class TwitterSearchResult
    {
        [JsonProperty("statuses")]
        public List<Status> Status { get; set; }
    }
}
