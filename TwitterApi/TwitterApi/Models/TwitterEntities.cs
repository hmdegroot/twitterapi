﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TwitterApi.Models
{
    public class TwitterEntities
    {
        [JsonProperty("hashtags")]
        public List<object> HashTags { get; set; }
        [JsonProperty("symbols")]
        public List<object> Symbols { get; set; }
        [JsonProperty("user_mentions")]
        public List<UserMentions> UserMentions { get; set; }
    }
}
