﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TwitterApi.Models
{
    public class Status
    {
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
        [JsonProperty("entities")]
        public TwitterEntities Entities { get; set; }
    }
}
