﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TwitterApi.Utilities
{
    public static class JsonDeserializer
    {
        public static TResponse DeserializeJsonString<TResponse>(string jsonString)
        {
            return JsonConvert.DeserializeObject<TResponse>(jsonString, new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Ignore
            });
        }
    }
}
