using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TwitterApi.DataAccessor;
using TwitterApi.DataAccessor.TwitterClient;
using TwitterApi.Managers;

namespace TwitterApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMvc().AddNewtonsoftJson();
            services.AddScoped<ITwitterSearchManager, TwitterSearchManager>();
            services.AddScoped<ITwitterSearchService, TwitterSearchService>();
            services.AddScoped<ITwitterClient, TwitterClient>();

            //services.AddHttpClient<ITwitterSearchService, TwitterSearchService>(client =>
            //{
            //    var bearerToken = "AAAAAAAAAAAAAAAAAAAAAIXNMgEAAAAAi8%2B%2B3X0L9VqPu8kExfr54OfmADU%3DwunSVsvBug9877IXrd6HDnJKKOo149dQvHhKI63S1EMAX92FCL";

            //    client.DefaultRequestHeaders.Clear();
            //    client.DefaultRequestHeaders.Add("Authorization", $"Bearer {bearerToken}");
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
