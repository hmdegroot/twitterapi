﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace TwitterApi.Managers
{
    public static class TwitterSearchStaticMethods
    {
        //as the api evolved, this process would likely change into something more reusible 
        public static string BuildSearchUri(string queryString)
        {
            if (string.IsNullOrWhiteSpace(queryString))
            {
                return $"https://api.twitter.com/1.1/search/tweets.json?q={queryString}";
            }

            var encodedString = WebUtility.UrlEncode(queryString);

            return $"https://api.twitter.com/1.1/search/tweets.json?q={encodedString}";
        }
    }
}
