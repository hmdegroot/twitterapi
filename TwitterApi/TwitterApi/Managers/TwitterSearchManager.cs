﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TwitterApi.DataAccessor;
using TwitterApi.Models;
using TwitterApi.Utilities;
using System.Net;

namespace TwitterApi.Managers
{
    public class TwitterSearchManager : ITwitterSearchManager
    {
        private readonly ITwitterSearchService _twitterService;

        public TwitterSearchManager(ITwitterSearchService twitterService)
        {
            _twitterService = twitterService;
        }

        public async Task<TwitterSearchResult> GetTwitterSearchResults(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return null;

            var uri = TwitterSearchStaticMethods.BuildSearchUri(query);

            var httpResponse = await _twitterService.SearchTweets(uri);

            var jsonString = await httpResponse.Content.ReadAsStringAsync();

            var deserializedResponse = JsonDeserializer.DeserializeJsonString<TwitterSearchResult>(jsonString);

            return deserializedResponse;
        }
    }
}
