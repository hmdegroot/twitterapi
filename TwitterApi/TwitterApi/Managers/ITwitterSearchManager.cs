﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TwitterApi.Models;

namespace TwitterApi.Managers
{
    public interface ITwitterSearchManager
    {
        public Task<TwitterSearchResult> GetTwitterSearchResults(string query);
    }
}
